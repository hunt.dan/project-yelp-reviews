# project-yelp-reviews
Analysis of the Yelp reviews dataset using AWS tools, Snowflake, and dbt.

For more detail on the data transformations and datasets, see the dbt readme under `/snowflake-models`.
## Requirements
Local AWS configuration via:
```
~/.aws/config
~/.aws/credentials
```

Yelp reviews dataset, which should be uploaded to S3 (optionally decompress first to skip next step, depending on bandwidth / cost concerns):
```zsh
aws s3 cp ~/Downloads/yelp_dataset.tar s://yelp-reviews-raw/compressed/yelp_dataset.tar
```

... and then untarred using [AWS Cloudshell](https://console.aws.amazon.com/cloudshell) (this took 5-10mins):
```zsh
aws s3 cp s3://yelp-reviews-raw/compressed/yelp_dataset.tar - | tar -xz --to-command='aws s3 cp - s3://yelp-reviews-raw/decompressed/$TAR_REALNAME'
```