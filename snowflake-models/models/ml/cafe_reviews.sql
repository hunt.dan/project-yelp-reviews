with reviews as (
  select * from {{ ref('fct_reviews') }}
),

businesses as (
  select * from {{ ref('dim_businesses') }}
),

relevant_business_ids as (
  select distinct business_id
  from businesses
    -- flatten string list of categories across rows
    , lateral split_to_table(businesses.categories_concatenated, ', ')

  -- keep only businesses with at least one relevant cafe-adjacent categories
  where value in ('Breakfast & Brunch','Coffee & Tea','Cafes','Bakeries','Diners','Delis','Gastropubs','Coffee Roasteries','Patisserie/Cake Shop','Coffeeshops')
),

relevant_reviews as (
  select *
  from reviews

  -- filter to only reviews related to relevant businesses
  where business_id in (select business_id from relevant_business_ids)
)

select * from relevant_reviews
