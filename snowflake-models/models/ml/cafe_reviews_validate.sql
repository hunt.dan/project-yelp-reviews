/*
Here we use a repeatable random sample so that model results can be iterated on
and tinkered with, without too many moving parts.

In a production setting, this may not be advisable since model drift or overfitting
may become a problem.
*/

with cafe_reviews as (
  select * from {{ ref('cafe_reviews') }}
),

reviews_train as (
  select * from {{ ref('cafe_reviews_train') }}
),

cafe_reviews_minus_train as (
  select * from cafe_reviews
  minus
  select * from reviews_train
)

select *
-- keep a random 50% sample of non-training data that is repeatable (for consistent ML model training)
from cafe_reviews_minus_train sample (50) repeatable (5)