/*
Here we use a repeatable random sample so that model results can be iterated on
and tinkered with, without too many moving parts.

In a production setting, this may not be advisable since model drift or overfitting
may become a problem.
*/

with cafe_reviews as (
  select * from {{ ref('cafe_reviews')}}
)

select *
-- keep a random 60% sample that is repeatable (for consistent ML model training)
from cafe_reviews sample (60) repeatable (5)
