-- split and flatten business categories and keep only businesses in relevant categories
with tips as (
  select * from {{ ref('base_tips') }}
),

reviews as (
  select * from {{ ref('fct_reviews') }}
),

business_has_specialty_reviews as (
  select
    business_id
    , max(is_specialty_cafe) as is_specialty_cafe
  from reviews
  group by 1
),

augmented_tips as (
  select tips.*
    , business_has_specialty_reviews.is_specialty_cafe
  from tips
  left join business_has_specialty_reviews
    on tips.business_id = business_has_specialty_reviews.business_id
)

select *
from augmented_tips
