-- split and flatten business categories and keep only businesses in relevant categories
with reviews as (
  select * from {{ ref('base_reviews') }}
),

reviews_augmented as (
  select
    reviews.*

    -- add label to reviews based on coarse filter
    , case
        when
            review_text ilike '%speciality coffee%'
            or review_text ilike '%cold brew%coffee%'
            or review_text ilike '%coffee%cold brew%'
            or review_text ilike '%cold drip%coffee%'
            or review_text ilike '%coffee%cold drip%'
            or review_text ilike '%light roast%coffee%'
            or review_text ilike '%latte art%'
            or review_text ilike '%flat white%coffee%'
            or review_text ilike '%single origin%coffee%'
            or review_text ilike '%coffee%single origin%'
            or review_text ilike '%single-origin%'
        then true
        else false
        end as is_specialty_cafe -- is_specialty_coffee
  from reviews
)

select * from reviews_augmented
