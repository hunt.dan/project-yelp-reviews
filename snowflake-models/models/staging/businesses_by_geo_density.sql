-- compute voronoi diagram, which represents the distance between points
with businesses as (
  select * from {{ ref('dim_businesses') }}  sample ( 1 ) repeatable (5)
),

customer_access_rules as (
  select * from {{ ref('sharing_cafe_access_rules_by_state') }}
),

cafes as (
  select
    -- retain high-level metadata to make data interpretable
    business_id
    , state
    , city

    -- convert long, lat data to native GEOGRAPHY type
    , to_geography('point(' || longitude || ' ' || latitude ||')') as geog
  from businesses
  where geog is not null
),

cafes_array as (
  select
    state
    , city

    -- create a list of all of the points in this city so that we can calculate an envelope
    , array_agg(st_asgeojson(geog)::string) as geog_array
  from cafes
  group by state, city
  
  -- don't compute a map when there are less than five businesses in a city - not useful
  having count(*) >= 5
),

voronoi_array as (
  select
    -- calculate a voronoi array for each city, in order to provide useful density data
    state
    , city
    
    -- calculate an "envelope" around the set of points to work within (boundary constraint)
    , carto_os_temp.carto.st_envelope_arr(geog_array) as envelope
    
    -- calculate the voronoi diagram, using the envelope above
    , carto_os_temp.carto.st_voronoipolygons(
        geog_array
        , array_construct(st_xmin(envelope)
            , st_ymin(envelope)
            , st_xmax(envelope)
            , st_ymax(envelope)
            )
        ) as nested_voronoi
  from cafes_array
),

cleaned as (
  select
    state
    , city

    -- data related to each voronoi diagram (for a single city)
    , md5(state || city) as voronoi_diagram_id -- create a hash for each city ID
    , envelope as voronoi_diagram_polygon
    , st_area(envelope) as voronoi_diagram_area
    
    -- data related to a single cell in the diagram (for a single business)
    , index as voronoi_cell_id
    , value
    , to_geography(value) as voronoi_cell_polygon
    , st_area(voronoi_cell_polygon) as voronoi_cell_area
    , path as voronoi_cell_path_id

    -- calculate the area of this cell relative to the city's bounding area
    -- high percentage cachement area represents an area that is a candidate for new stores
    , voronoi_cell_area / voronoi_diagram_area as voronoi_cell_pct_diagram_area

    -- create a unique surrogate key for the table to test
    , md5(voronoi_diagram_id || voronoi_cell_id) as unique_id
  from voronoi_array
    , lateral flatten(input => nested_voronoi)  
)

select * from cleaned
