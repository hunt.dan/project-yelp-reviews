
select review_id
  , user_id
  , business_id

  -- qualify these column names to provide clarity later after joins
  , stars as review_stars
  , useful as review_useful
  , funny as review_funny
  , cool as review_cool
  , text as review_text
  , date::date as review_date
from {{ source('yelp', 'reviews') }}
