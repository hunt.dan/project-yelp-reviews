
select
  business_id
  , name as business_name
  , address as business_address
  , city
  , state
  , postal_code
  , latitude
  , longitude
  , stars as business_average_star_rating
  , review_count as business_total_review_count
  , is_open::boolean as is_open
  , categories as categories_concatenated

  -- split this string into a more structured format
  , split(categories, ', ') as categories_array
from {{ source('yelp', 'businesses') }}
