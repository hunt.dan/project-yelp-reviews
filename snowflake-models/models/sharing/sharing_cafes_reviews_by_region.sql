{{
  config(
    materialized='view',
    secure=true,
    post_hook="{{ share_secure_view('customer_share') }}"
  )
}}

with reviews_denormalised as (
  select * from {{ ref('reviews_denormalised') }}
),

customer_access_rules as (
  select * from {{ ref('sharing_cafe_access_rules_by_state') }}
),

dataset_permissions as (
  select
    -- data API columns
    reviews_denormalised.*

    -- customer_id, which can be used for permissions
    , customer_access_rules.customer_id
  from reviews_denormalised
  join customer_access_rules
    -- customers buy access to data for specific states, encoded via these permissions
    -- this will effectively duplicate the mart data for each customer (although this is a view)
    on customer_access_rules.state = reviews_denormalised.state

  -- macro that will restrict data per customer using secure views and the current_account() function
  where {{
    apply_share_row_level_security(
      id_column='customer_id'
      , access_rules_id_column='customer_id'
      , access_rules_ref='sharing_cafe_access_rules_by_state'
    )
  }}
)

select * from dataset_permissions
