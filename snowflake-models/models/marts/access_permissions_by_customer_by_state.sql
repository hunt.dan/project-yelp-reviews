
select 
  aws_quicksight_username as "UserName" -- case sensitive

  -- this is how access to data will be granted at a row level
  , state
from {{ ref('sharing_cafe_access_rules_by_state') }}
