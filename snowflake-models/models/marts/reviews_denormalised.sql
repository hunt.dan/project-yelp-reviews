-- materialize as table to avoid share cross-database issues
{{
  config(
    materialized='table'
  )
}}


-- split and flatten business categories and keep only businesses in relevant categories
with reviews as (
  select * from {{ ref('fct_reviews') }}
),

businesses as (
  select * from {{ ref('dim_businesses') }}
),

reviews_augmented as (
  select
    reviews.*

    -- added columns from dim_businesses
    , business_name
    , business_address
    , city
    , state
    , postal_code
    , latitude
    , longitude
    , business_average_star_rating
    , business_total_review_count
    , is_open
    , categories_concatenated

  from reviews
  inner join businesses using (business_id)
)

select * from reviews_augmented
