# Snowflake models
This project represents the data transformations that convert raw data from various systems (loaded into the `raw` database in Snowflake) into analytical tables used for analysis, visualisation, and general downstream consumption.

In other words, this represents the in-warehouse transformation layer of the data stack.

## Getting set up with dbt
This repository integrates with build tool [dbt](https://docs.getdbt.com/), which creates directed acyclic graphs (DAGs) of data models based on their dependencies, and can be used to build or update these tables from source.

### Using the starter project

Try running the following commands:
- dbt run
- dbt test


### dbt Resources:
- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](http://slack.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices
