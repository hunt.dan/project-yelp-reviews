{% macro apply_share_row_level_security(id_column, access_rules_id_column, access_rules_ref) %}
{{ id_column }} in (
  select {{ access_rules_id_column }} 
  from {{ ref(access_rules_ref) }}
  where account_locator = current_account()
)
{% endmacro %}
