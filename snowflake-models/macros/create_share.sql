-- Thanks to the Drizly team for this macro and approach!
{% macro create_share(share_name, access_rules_ref) %}
  {% set current_role = run_query('select current_role()') %}
  {% if (execute and target.name == "prod" and
         current_role.columns[0].values()[0] == 'DBT_USER') %}
    {% set sql %}
      create share if not exists {{ share_name }};
      grant usage on database {{ target.database }} to share {{ share_name }};

      -- some models reference data from the raw database, which requires
      -- the reference_usage grant
      grant reference_usage on database raw to share {{ share_name }};

      -- get accounts from the access rules model to add to the share
      {% set accounts_query %}
        select distinct account_locator from {{ ref(access_rules_ref) }}
      {% endset %}
      {{ log("Getting accounts to add to share with: " ~ accounts_query) }}
      {% set accounts = dbt.run_query(accounts_query) %}
      {% set accounts_list = accounts.columns[0].values() %}

      {% for account in accounts_list %}
        {{ log("Adding account " ~ account ~ ' to share ' ~ share_name, info=True) }}
        alter share {{ share_name }} add accounts = {{ account }};
      {% endfor %}
    {% endset %}
    {% set table = run_query(sql) %}
  {% endif %}
{% endmacro %}
