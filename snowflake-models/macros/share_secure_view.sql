{% macro share_secure_view(share_name, expected_target_schema='PROD') %}
  {% set current_role = run_query('select current_role()') %}
  {% if (execute and target.name == 'prod' and
          current_role.columns[0].values()[0] == 'DBT_USER') %}
    {% if target.schema == expected_target_schema %}
      {% set sql %}
        {{ log("Granting usage on schema " ~ this.database ~ "." ~ this.schema
              ~ " to share " ~ share_name, info=True) }}
        grant usage
        on schema {{ this.database }}.{{ this.schema }}
        to share {{ share_name }};

        {{ log("Granting select on secure view " ~ this ~ " to share " ~
                share_name, info=True) }}
        grant select
        on view {{ this }}
        to share {{ share_name }};
      {% endset %}
      {% set table = run_query(sql) %}
    {% else %}
      {{ log("Skipping sharing of " ~ this ~
            " due to expected target schema mismatch. Expected " ~
            expected_target_schema ~ " but found " ~ target.schema ~
            " (this is normal behaviour in dbt cloud jobs for PRs)", info=True) }}
    {% endif %}
  {% endif %}
{% endmacro %}