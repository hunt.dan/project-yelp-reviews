import { Construct } from "constructs";
import {
  Duration,
  aws_s3,
  aws_glue,
  aws_iam,
  // aws_lambda,
  // aws_events,
  // aws_sagemaker,
  // aws_secretsmanager,
  // aws_events_targets,
} from "aws-cdk-lib";
import { AwsTerraformAdapter, AwsProvider, glue } from "@cdktf/aws-cdk";
import { App, TerraformStack, RemoteBackend } from "cdktf";
import { registerMapping } from "@cdktf/aws-cdk/lib/mapping";

// manually map currently unsupported cdktf resources for a Glue Crawler
registerMapping("AWS::Glue::Crawler", {
  resource: (scope: Construct, id: string, props: any) => {
    console.log({props});
    console.log(props.Targets.S3Targets);
    const mappedProps: glue.GlueCrawlerConfig = {
      name: props.Name,
      role: props.Role,
      databaseName: props.DatabaseName,
      tablePrefix: props.TablePrefix,
      s3Target: props.Targets.S3Targets.map(
        (s3Target: any) => ({ path: s3Target.Path })
      ), // this allows multiple s3 targets
    };
    // delete props we successfully mapped to mark them as handled
    delete props.Name;
    delete props.Role;
    delete props.DatabaseName;
    delete props.TablePrefix;
    delete props.Targets; // for now, we don't support any non-S3 targets

    return new glue.GlueCrawler(scope, id, mappedProps);
  },
  // may need to map attributes if referring to this object elsewhere
  attributes: {},
});

class MyStack extends TerraformStack {
  constructor(scope: Construct, id: string) {
    super(scope, id);

    new AwsProvider(this, "aws", {
      region: "eu-west-1",
      profile: "personal",
    });

    const awsAdapter = new AwsTerraformAdapter(this, "adapter");

    const rawBucket = new aws_s3.Bucket(awsAdapter, "yelp-reviews-raw", {
      bucketName: "yelp-reviews-raw",
      blockPublicAccess: aws_s3.BlockPublicAccess.BLOCK_ALL,
      lifecycleRules: [
        { expiration: Duration.days(365) },
      ]
    });

    // set up the target for the first glue crawl
    const s3RawTargetProperty: aws_glue.CfnCrawler.TargetsProperty = {
      s3Targets: [
        {
          path: rawBucket.s3UrlForObject(), // crawl entire bucket
        },
      ],
    }

    const glueRole = new aws_iam.Role(awsAdapter, "yelp-glue", {
      assumedBy: new aws_iam.ServicePrincipal('glue.amazonaws.com'),
      description: 'Role to crawl S3 buckets and create Glue databases',
    });

    rawBucket.grantRead(glueRole);

    new aws_glue.CfnCrawler(awsAdapter, "yelp-reviews-raw-crawler", {
      name: "yelp-reviews-raw-crawler",
      role: glueRole.roleArn,
      databaseName: "yelp_reviews",
      tablePrefix: "yelp_",
      targets: s3RawTargetProperty,
    });
  }
}

const app = new App();
const stack = new MyStack(app, "infrastructure");

// this is where terraform state will be stored (remotely in this case)
new RemoteBackend(stack, {
  hostname: "app.terraform.io",
  organization: "yelp-reviews",
  workspaces: {
    name: "snowflake-demo"
  }
});
app.synth();
